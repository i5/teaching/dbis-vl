FasdUAS 1.101.10   ��   ��    k             l     ��  ��      2018-01-31     � 	 	    2 0 1 8 - 0 1 - 3 1   
  
 l     ��  ��    _ Y https://github.com/irmowan/Convert-ppt-to-pdf/blob/master/Convert-ppt-to-pdf.applescript     �   �   h t t p s : / / g i t h u b . c o m / i r m o w a n / C o n v e r t - p p t - t o - p d f / b l o b / m a s t e r / C o n v e r t - p p t - t o - p d f . a p p l e s c r i p t      l     ��  ��       modified by WF 2020-11-15     �   4   m o d i f i e d   b y   W F   2 0 2 0 - 1 1 - 1 5      l     ��  ��    9 3 see https://apple.stackexchange.com/a/406988/57019     �   f   s e e   h t t p s : / / a p p l e . s t a c k e x c h a n g e . c o m / a / 4 0 6 9 8 8 / 5 7 0 1 9      l     ��  ��           �           l     ��   !��     T N ppt2Pdf({"/Users/wf/Projekte/2020/Infrastruktur2020/ppt2pdf", "TestMe.pptx"})    ! � " " �   p p t 2 P d f ( { " / U s e r s / w f / P r o j e k t e / 2 0 2 0 / I n f r a s t r u k t u r 2 0 2 0 / p p t 2 p d f " ,   " T e s t M e . p p t x " } )   # $ # l     ��������  ��  ��   $  % & % l     �� ' (��   ' C =http://hints.macworld.com/article.php?story=20050523140439734    ( � ) ) z h t t p : / / h i n t s . m a c w o r l d . c o m / a r t i c l e . p h p ? s t o r y = 2 0 0 5 0 5 2 3 1 4 0 4 3 9 7 3 4 &  * + * l     �� , -��   , 4 . passing command line arguments to applescript    - � . . \   p a s s i n g   c o m m a n d   l i n e   a r g u m e n t s   t o   a p p l e s c r i p t +  / 0 / i      1 2 1 I     �� 3��
�� .aevtoappnull  �   � **** 3 l      4���� 4 o      ���� 0 argv  ��  ��  ��   2 k     3 5 5  6 7 6 Z      8 9���� 8 l     :���� : =      ; < ; o     ���� 0 argv   <  f    ��  ��   9 l    = > ? = r     @ A @ J    
 B B  C D C m     E E � F F @ / U s e r s / w f / s o u r c e / d b i s - v l / d b i s - v l D  G�� G m     H H � I I @ 2 - E R M / 2 - E n t i t y - R e l a t i o n s h i p . p p t x��   A o      ���� 0 argv   >  	 whatever    ? � J J    w h a t e v e r��  ��   7  K L K I   �� M��
�� .ascrcmnt****      � **** M l    N���� N I   �� O��
�� .corecnte****       **** O l    P���� P o    ���� 0 argv  ��  ��  ��  ��  ��  ��   L  Q�� Q Z    3 R S�� T R l   " U���� U A    " V W V l     X���� X I    �� Y��
�� .corecnte****       **** Y o    ���� 0 argv  ��  ��  ��   W m     !���� ��  ��   S I  % *�� Z��
�� .ascrcmnt****      � **** Z m   % & [ [ � \ \ F u s a g e :   p p t 2 p d f   b a s e p a t h   [ f i l e n a m e s ]��  ��   T n  - 3 ] ^ ] I   . 3�� _���� 0 ppt2pdf ppt2Pdf _  `�� ` o   . /���� 0 argv  ��  ��   ^  f   - .��   0  a b a l     ��������  ��  ��   b  c d c l     ��������  ��  ��   d  e f e l     �� g h��   g ; 5 convert powerpoint to pdf on the given list of files    h � i i j   c o n v e r t   p o w e r p o i n t   t o   p d f   o n   t h e   g i v e n   l i s t   o f   f i l e s f  j k j l     ��������  ��  ��   k  l m l i     n o n I      �� p���� 0 ppt2pdf ppt2Pdf p  q�� q o      ���� 0 	filenames 	fileNames��  ��   o k    B r r  s t s I    �� u��
�� .ascrcmnt****      � **** u m      v v � w w 0 l a u n c h i n g   P o w e r p o i n t   . . .��   t  x y x r    	 z { z m     | | � } } ( M i c r o s o f t   P o w e r P o i n t { o      ���� 0 pp   y  ~�� ~ O   
B  �  l  A � � � � k   A � �  � � � I   ������
�� .ascrnoop****      � ****��  ��   �  � � � r     � � � m    ��
�� boovtrue � o      ���� 0 isfirst   �  � � � X   ; ��� � � Z   +6 � ��� � � o   + ,���� 0 isfirst   � k   / > � �  � � � r   / 2 � � � o   / 0���� 0 filename fileName � o      ���� 0 basepath   �  � � � I  3 :�� ���
�� .ascrcmnt****      � **** � b   3 6 � � � m   3 4 � � � � �  b a s e   p a t h   i s   � o   4 5���� 0 basepath  ��   �  ��� � r   ; > � � � m   ; <��
�� boovfals � o      ���� 0 isfirst  ��  ��   � Z   A6 � ����� � G   A T � � � G   A L � � � D   A D � � � o   A B���� 0 filename fileName � m   B C � � � � �  . p p t � D   G J � � � o   G H���� 0 filename fileName � m   H I � � � � � 
 . p p t x � D   O R � � � o   O P���� 0 filename fileName � m   P Q � � � � � 
 . p p t m � k   W2 � �  � � � r   W ^ � � � b   W \ � � � b   W Z � � � o   W X���� 0 basepath   � m   X Y � � � � �  / � o   Z [���� 0 filename fileName � o      ���� 0 filepath filePath �  � � � l  _ _�� � ���   � . ( set filePath to POSIX path of fileAlias    � � � � P   s e t   f i l e P a t h   t o   P O S I X   p a t h   o f   f i l e A l i a s �  � � � r   _ g � � � n  _ e � � � I   ` e�� ����� 0 makenewpath makeNewPath �  ��� � o   ` a���� 0 filepath filePath��  ��   �  f   _ ` � o      ���� 0 pdfpath pdfPath �  � � � I  h u�� ���
�� .ascrcmnt****      � **** � b   h q � � � b   h o � � � b   h k � � � m   h i � � � � � D t r y i n g   t o   c o n v e r t   p o w e r p o i n t   f i l e   � o   i j���� 0 filepath filePath � m   k n � � � � �    t o   � o   o p���� 0 pdfpath pdfPath��   �  � � � I  v {�� ���
�� .aevtodocnull  �    alis � o   v w���� 0 filepath filePath��   �  � � � l  | |��������  ��  ��   �  � � � l  | |�� � ���   � : 4 save active presentation in pdfPath as save as PDF     � � � � h   s a v e   a c t i v e   p r e s e n t a t i o n   i n   p d f P a t h   a s   s a v e   a s   P D F   �  � � � l  | |�� � ���   �   save in same folder    � � � � (   s a v e   i n   s a m e   f o l d e r �  � � � l  | |�� � ���   � 5 / https://macscripter.net/viewtopic.php?id=26342    � � � � ^   h t t p s : / / m a c s c r i p t e r . n e t / v i e w t o p i c . p h p ? i d = 2 6 3 4 2 �  � � � l  | |�� � ���   � &  tell application "System Events"    � � � � @ t e l l   a p p l i c a t i o n   " S y s t e m   E v e n t s " �  � � � l  | |�� � ���   � T N	set listOfProcesses to (name of every process where background only is false)    � � � � � 	 s e t   l i s t O f P r o c e s s e s   t o   ( n a m e   o f   e v e r y   p r o c e s s   w h e r e   b a c k g r o u n d   o n l y   i s   f a l s e ) �  � � � l  | |�� � ���   � l f	tell me to set selectedProcesses to choose from list listOfProcesses with multiple selections allowed    � � � � � 	 t e l l   m e   t o   s e t   s e l e c t e d P r o c e s s e s   t o   c h o o s e   f r o m   l i s t   l i s t O f P r o c e s s e s   w i t h   m u l t i p l e   s e l e c t i o n s   a l l o w e d �  � � � l  | |�� � ���   �  end tell    � � � �  e n d   t e l l �    l  | |����   2 ,repeat with processName in selectedProcesses    � X r e p e a t   w i t h   p r o c e s s N a m e   i n   s e l e c t e d P r o c e s s e s  l  | |����    	log processName    �		   	 l o g   p r o c e s s N a m e 

 l  | |����    
end repeat    �  e n d   r e p e a t  l  | |����   &   Datei/Drucken -> Ablage/Drucken    � @   D a t e i / D r u c k e n   - >   A b l a g e / D r u c k e n  Z   | ����� H   | � n  | � I   } �������  0 choosemenuitem chooseMenuItem  o   } ~���� 0 pp    m   ~ �   �!!  A b l a g e "��" m   � �## �$$  D r u c k e n . . .��  ��    f   | } k   � �%% &'& I  � ���(��
�� .ascrcmnt****      � ****( m   � �)) �** : A b l a g e / D r u c k e n . . .   c a l l   f a i l e d��  ' +��+ R   � ����,
�� .ascrerr ****      � ****�  , �~-�}
�~ 
errn- m   � ��|�|���}  ��  ��  ��   ./. I  � ��{0�z
�{ .ascrcmnt****      � ****0 m   � �11 �22 4 A b l a g e / D r u c k e n   m e n u   o p e n e d�z  / 343 l  � ��y56�y  5 * $my showUiElements(pp, "menu button")   6 �77 H m y   s h o w U i E l e m e n t s ( p p ,   " m e n u   b u t t o n " )4 898 l  � ��x:;�x  : < 6 my waitFor(button whose description is "PDF", 5, 0.5)   ; �<< l   m y   w a i t F o r ( b u t t o n   w h o s e   d e s c r i p t i o n   i s   " P D F " ,   5 ,   0 . 5 )9 =>= l  � ��w?@�w  ? - 'my choosePopUp(pp, "Layout", "Notizen")   @ �AA N m y   c h o o s e P o p U p ( p p ,   " L a y o u t " ,   " N o t i z e n " )> BCB l  � ��vDE�v  D , &my choosePopUp(pp, "Ausgabe", "Farbe")   E �FF L m y   c h o o s e P o p U p ( p p ,   " A u s g a b e " ,   " F a r b e " )C GHG l  � ��uIJ�u  I ; 5my chooseMenuButtonItem(pp, "PDF", "Als PDF sichern")   J �KK j m y   c h o o s e M e n u B u t t o n I t e m ( p p ,   " P D F " ,   " A l s   P D F   s i c h e r n " )H LML q   � �NN �t�s�t 0 mytitle myTitle�s  M OPO O   � �QRQ k   � �SS TUT l  � ��rVW�r  V   the magic of Applescript   W �XX 2   t h e   m a g i c   o f   A p p l e s c r i p tU YZY l  � ��q[\�q  [ Y S if you really want the title and not a reference to it you need to use an operator   \ �]] �   i f   y o u   r e a l l y   w a n t   t h e   t i t l e   a n d   n o t   a   r e f e r e n c e   t o   i t   y o u   n e e d   t o   u s e   a n   o p e r a t o rZ ^_^ l  � ��p`a�p  ` a [ http://books.gigatux.nl/mirror/applescriptdefinitiveguide/applescpttdg2-CHP-12-SECT-5.html   a �bb �   h t t p : / / b o o k s . g i g a t u x . n l / m i r r o r / a p p l e s c r i p t d e f i n i t i v e g u i d e / a p p l e s c p t t d g 2 - C H P - 1 2 - S E C T - 5 . h t m l_ c�oc r   � �ded b   � �fgf n   � �hih 1   � ��n
�n 
titli n   � �jkj 4   � ��ml
�m 
cwinl m   � ��l�l k 4   � ��km
�k 
prcsm o   � ��j�j 0 pp  g m   � �nn �oo  e o      �i�i 0 mytitle myTitle�o  R m   � �pp�                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  P qrq n  � �sts I   � ��hu�g�h ,0 choosemenubuttonitem chooseMenuButtonItemu vwv o   � ��f�f 0 pp  w xyx m   � �zz �{{  P D Fy |�e| m   � �}} �~~ $ I n   V o r s c h a u   � f f n e n�e  �g  t  f   � �r � I  � ��d��c
�d .sysodelanull��� ��� nmbr� m   � ��b�b �c  � ��� O   �(��� k   �'�� ��� I  � ��a��`
�a .ascrcmnt****      � ****� b   � ���� m   � ��� ��� @ w a i t i n g   f o r   V o r s c h a u   t o   d i s p l a y  � o   � ��_�_ 0 mytitle myTitle�`  � ��� r   � ���� n  � ���� I   � ��^��]�^ *0 waitforappearwindow waitForAppearWindow� ��� o   � ��\�\ 0 mytitle myTitle� ��� m   � ��� ���  V o r s c h a u� ��� m   � ��[�[ � ��Z� m   � ��� ?�      �Z  �]  �  f   � �� o      �Y�Y 0 timeleft timeLeft� ��X� Z   �'���W�� A   ���� o   � �V�V 0 timeleft timeLeft� m   �U�U  � k  �� ��� I �T��S
�T .ascrcmnt****      � ****� b  ��� b  
��� m  �� ���  V o r s c h a u  � o  	�R�R 0 mytitle myTitle� m  
�� ��� H   w i n d o w   d i d n ' t   s h o w   u p   a f t e r   3 0   s e c s�S  � ��Q� R  �P�O�
�P .ascrerr ****      � ****�O  � �N��M
�N 
errn� m  �L�L���M  �Q  �W  � k   '�� ��� I  -�K��J
�K .ascrcmnt****      � ****� b   )��� b   %��� m   #�� ��� . V o r s c h a u   a p p e a r e d   w i t h  � o  #$�I�I 0 timeleft timeLeft� m  %(�� ���  s e c s   l e f t�J  � ��H� O  .'��� k  9&�� ��� I 9@�G��F
�G .sysodelanull��� ��� nmbr� m  9<�� ?ə������F  � ��� I A^�E��D
�E .prcsclicnull��� ��� uiel� n  AZ��� 4  SZ�C�
�C 
menI� m  VY�� ��� * A l s   P D F   e x p o r t i e r e n   &� n  AS��� 4  NS�B�
�B 
menE� m  QR�A�A � n  AN��� 4  GN�@�
�@ 
mbri� m  JM�� ���  A b l a g e� 4  AG�?�
�? 
mbar� m  EF�>�> �D  � ��� I _f�=��<
�= .sysodelanull��� ��� nmbr� m  _b�� ?�      �<  � ��� l gg�;���;  � - ' CMD-SHIFT-G to set the export director   � ��� N   C M D - S H I F T - G   t o   s e t   t h e   e x p o r t   d i r e c t o r� ��� l gg�:���:  � 9 3 https://dougscripts.com/itunes/itinfo/keycodes.php   � ��� f   h t t p s : / / d o u g s c r i p t s . c o m / i t u n e s / i t i n f o / k e y c o d e s . p h p� ��� I gy�9��
�9 .prcskprsnull���     ctxt� m  gj�� ���  g� �8��7
�8 
faal� J  mu�� ��� m  mp�6
�6 eMdsKcmd� ��5� m  ps�4
�4 eMdsKsft�5  �7  � ��� I z��3��2
�3 .sysodelanull��� ��� nmbr� m  z}�� ?ə������2  � ��� l ���1�0�/�1  �0  �/  � ��� O  �$��� k  �#�� ��� O  ��� � k  ��  l ���.�.     dereference basePath    � *   d e r e f e r e n c e   b a s e P a t h  q  ��		 �-�,�- 0 basepathstr basePathStr�,   

 r  �� b  �� o  ���+�+ 0 basepath   m  �� �   o      �*�* 0 basepathstr basePathStr  r  �� o  ���)�) 0 basepathstr basePathStr n       1  ���(
�( 
valL 4 ���'
�' 
comB m  ���&�&   I ���%�$
�% .sysodelanull��� ��� nmbr m  �� ?ə������$    I ���#�"
�# .prcsclicnull��� ��� uiel 4  ���! 
�! 
butT  m  ��!! �""  � f f n e n�"   #� # I ���$�
� .sysodelanull��� ��� nmbr$ m  ��%% ?ə������  �     4  ���&
� 
sheE& m  ���� � '(' I ���)�
� .prcsclicnull��� ��� uiel) 4  ���*
� 
butT* m  ��++ �,,  S i c h e r n�  ( -.- r  ��/0/ n ��121 I  ���3�� 0 waitforappear waitForAppear3 454 4  ���6
� 
sheE6 m  ���� 5 787 m  ���� 8 9�9 m  ��:: ?ə������  �  2  f  ��0 o      �� 0 timeleft timeLeft. ;<; O  �=>= Z  �
?@��? ?  ��ABA o  ���� 0 timeleft timeLeftB m  ����  @ I ��C�
� .prcsclicnull��� ��� uielC 4  ��D
� 
butTD m  �EE �FF  E r s e t z e n�  �  �  > 4  ���
G
�
 
sheEG m  ���	�	 < HIH I �J�
� .sysodelanull��� ��� nmbrJ m  �� �  I K�K I #�LM
� .prcskprsnull���     ctxtL m  NN �OO  qM �P�
� 
faalP J  QQ R�R m  � 
�  eMdsKcmd�  �  �  � n  ��STS 4  ����U
�� 
sheEU m  ������ T 4  ����V
�� 
cwinV l ��W����W o  ������ 0 mytitle myTitle��  ��  � X��X l %%��������  ��  ��  ��  � 4  .6��Y
�� 
prcsY m  25ZZ �[[  V o r s c h a u�H  �X  � m   � �\\�                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  � ]^] l ))��������  ��  ��  ^ _`_ l ))��ab��  a &  tell application "System Events"   b �cc @ t e l l   a p p l i c a t i o n   " S y s t e m   E v e n t s "` ded l ))��fg��  f { u	set timeLeft to my waitForAppear("button", button "Sichern" of sheet 1 of sheet 1 of window 1 of process pp, 5, 0.5)   g �hh � 	 s e t   t i m e L e f t   t o   m y   w a i t F o r A p p e a r ( " b u t t o n " ,   b u t t o n   " S i c h e r n "   o f   s h e e t   1   o f   s h e e t   1   o f   w i n d o w   1   o f   p r o c e s s   p p ,   5 ,   0 . 5 )e iji l ))��kl��  k  	if timeLeft < 0 then   l �mm * 	 i f   t i m e L e f t   <   0   t h e nj non l ))��pq��  p 8 2		log "Sichern button didn't show up after 5 secs"   q �rr d 	 	 l o g   " S i c h e r n   b u t t o n   d i d n ' t   s h o w   u p   a f t e r   5   s e c s "o sts l ))��uv��  u  		error number -128   v �ww & 	 	 e r r o r   n u m b e r   - 1 2 8t xyx l ))��z{��  z  	end if   { �||  	 e n d   i fy }~} l ))�����   M G	click button "Sichern" of sheet 1 of sheet 1 of window 1 of process pp   � ��� � 	 c l i c k   b u t t o n   " S i c h e r n "   o f   s h e e t   1   o f   s h e e t   1   o f   w i n d o w   1   o f   p r o c e s s   p p~ ��� l ))������  �  end tell   � ���  e n d   t e l l� ��� l ))��������  ��  ��  � ��� l ))������  � &  tell application "System Events"   � ��� @ t e l l   a p p l i c a t i o n   " S y s t e m   E v e n t s "� ��� l ))������  �  
	delay 0.5   � ���  	 d e l a y   0 . 5� ��� l ))������  � 
 	try   � ���  	 t r y� ��� l ))������  � � �		set timeLeft to my waitForAppear("button", button "Ersetzen" of sheet 1 of sheet 1 of sheet 1 of window 1 of process pp, 5, 0.5)   � ��� 	 	 s e t   t i m e L e f t   t o   m y   w a i t F o r A p p e a r ( " b u t t o n " ,   b u t t o n   " E r s e t z e n "   o f   s h e e t   1   o f   s h e e t   1   o f   s h e e t   1   o f   w i n d o w   1   o f   p r o c e s s   p p ,   5 ,   0 . 5 )� ��� l ))������  �  		if timeLeft < 0 then   � ��� , 	 	 i f   t i m e L e f t   <   0   t h e n� ��� l ))������  � : 4			log "Ersetzen button didn't show up after 5 secs"   � ��� h 	 	 	 l o g   " E r s e t z e n   b u t t o n   d i d n ' t   s h o w   u p   a f t e r   5   s e c s "� ��� l ))������  �  			error number -128   � ��� ( 	 	 	 e r r o r   n u m b e r   - 1 2 8� ��� l ))������  �  		end if   � ���  	 	 e n d   i f� ��� l ))������  � Z T		click button "Ersetzen" of sheet 1 of sheet 1 of sheet 1 of window 1 of process pp   � ��� � 	 	 c l i c k   b u t t o n   " E r s e t z e n "   o f   s h e e t   1   o f   s h e e t   1   o f   s h e e t   1   o f   w i n d o w   1   o f   p r o c e s s   p p� ��� l ))������  �  	end try   � ���  	 e n d   t r y� ��� l ))������  �  end tell   � ���  e n d   t e l l� ��� I )0�����
�� .ascrcmnt****      � ****� m  ),�� ���  d o n e   . . .��  � ���� l 11������  �   close filePath   � ���    c l o s e   f i l e P a t h��  ��  ��  �� 0 filename fileName � o    ���� 0 	filenames 	fileNames � ��� l <<������  � ' ! still in tell powerpoint context   � ��� B   s t i l l   i n   t e l l   p o w e r p o i n t   c o n t e x t� ��� l <<������  � &  tell application "System Events"   � ��� @ t e l l   a p p l i c a t i o n   " S y s t e m   E v e n t s "� ��� l <<������  �  
	delay 0.5   � ���  	 d e l a y   0 . 5� ��� l <<������  � 
 	try   � ���  	 t r y� ��� l <<������  � Y S		set timeLeft to my waitForVanish("window", window "Sichern" of process pp, 60, 1)   � ��� � 	 	 s e t   t i m e L e f t   t o   m y   w a i t F o r V a n i s h ( " w i n d o w " ,   w i n d o w   " S i c h e r n "   o f   p r o c e s s   p p ,   6 0 ,   1 )� ��� l <<������  �  		if timeLeft < 0 then   � ��� , 	 	 i f   t i m e L e f t   <   0   t h e n� ��� l <<������  � 7 1			log "print dialog didn't vanish after 60 secs"   � ��� b 	 	 	 l o g   " p r i n t   d i a l o g   d i d n ' t   v a n i s h   a f t e r   6 0   s e c s "� ��� l <<������  �  			error number -128   � ��� ( 	 	 	 e r r o r   n u m b e r   - 1 2 8� ��� l <<������  �  		end if   � ���  	 	 e n d   i f� ��� l <<������  �  	end try   � ���  	 e n d   t r y� ��� l <<������  �  end tell   � ���  e n d   t e l l�  ��  I <A������
�� .aevtquitnull��� ��� null��  ��  ��   � %  work on version 15.15 or newer    � � >   w o r k   o n   v e r s i o n   1 5 . 1 5   o r   n e w e r � 4   
 ��
�� 
capp o    ���� 0 pp  ��   m  l     ��������  ��  ��    i     I      ��	���� 0 showelement showElement	 
��
 o      ���� 0 uielem uiElem��  ��   k       q       ������ 0 	classname 	className��    r      c      n      m    ��
�� 
pcls o     ���� 0 uielem uiElem m    ��
�� 
TEXT o      ���� 0 	classname 	className �� I   ����
�� .ascrcmnt****      � **** b     l   ���� b     b     l    ����  c    !"! b    #$# b    %&% l   '����' c    ()( n    *+* 1   	 ��
�� 
pDSC+ o    	���� 0 uielem uiElem) m    ��
�� 
TEXT��  ��  & m    ,, �--  =$ n    ./. o    ���� 	0 value  / o    ���� 0 uielem uiElem" m    ��
�� 
TEXT��  ��   m    00 �11  ( o    ���� 0 	classname 	className��  ��   m    22 �33  )��  ��   454 l     ��������  ��  ��  5 676 l     ��������  ��  ��  7 898 l     ��:;��  :   show all UI elements   ; �<< *   s h o w   a l l   U I   e l e m e n t s9 =>= l     ��������  ��  ��  > ?@? i    ABA I      ��C����  0 showuielements showUiElementsC DED o      ���� 0 appname appNameE F��F o      ���� "0 filterclassname filterClassName��  ��  B O     GHG O    ~IJI O    }KLK X    |M��NM Q   2 wOP��O k   5 nQQ RSR q   5 5TT ������ 0 	classname 	className��  S UVU r   5 <WXW c   5 :YZY n   5 8[\[ m   6 8��
�� 
pcls\ o   5 6���� 0 uielem uiElemZ m   8 9��
�� 
TEXTX o      ���� 0 	classname 	classNameV ]��] Z   = n^_����^ G   = H`a` =  = @bcb o   = >���� "0 filterclassname filterClassNamec m   > ?��
�� 
msnga =  C Fded o   C D���� 0 	classname 	classNamee o   D E���� $0 filterclasssname filterClasssname_ I  K j��f��
�� .ascrcmnt****      � ****f b   K fghg l  K bi����i b   K bjkj b   K `lml l  K \n����n c   K \opo b   K Zqrq b   K Vsts l  K Ru����u c   K Rvwv n   K Pxyx 1   L P��
�� 
descy o   K L���� 0 uielem uiElemw m   P Q��
�� 
TEXT��  ��  t m   R Uzz �{{  =r n   V Y|}| 1   W Y�
� 
valL} o   V W�~�~ 0 uielem uiElemp m   Z [�}
�} 
TEXT��  ��  m m   \ _~~ �  (k o   ` a�|�| 0 	classname 	className��  ��  h m   b e�� ���  )��  ��  ��  ��  P R      �{�z�y
�{ .ascrerr ****      � ****�z  �y  ��  �� 0 uielem uiElemN c   ! &��� n   ! $��� 1   " $�x
�x 
ects�  g   ! "� m   $ %�w
�w 
listL l   ��v�u� 6   ��� 4  �t�
�t 
cwin� m    �s�s � =   ��� n    ��� 1    �r
�r 
valL� 4    �q�
�q 
attr� m    �� ���  A X M a i n� m    �p
�p boovtrue�v  �u  J 4    �o�
�o 
prcs� o    �n�n 0 appname appNameH m     ���                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  @ ��� l     �m�l�k�m  �l  �k  � ��� l     �j�i�h�j  �i  �h  � ��� l     �g���g  � + % wait for the given element to appear   � ��� J   w a i t   f o r   t h e   g i v e n   e l e m e n t   t o   a p p e a r� ��� l     �f�e�d�f  �e  �d  � ��� i    ��� I      �c��b�c *0 waitforappearwindow waitForAppearWindow� ��� o      �a�a 0 elementname elementName� ��� o      �`�` 0 processname processName� ��� 1      �_
�_ 
time� ��^� o      �]�] 	0 slice  �^  �b  � k     T�� ��� r     ��� 1     �\
�\ 
time� o      �[�[ 0 timeleft timeLeft� ��� r    ��� m    �Z
�Z boovfals� o      �Y�Y 0 appeared  � ��� O    K��� W    J��� k    E�� ��� Q    3���X� r    *��� I   (�W��V
�W .coredoexnull���     ****� n    $��� 4   ! $�U�
�U 
cwin� o   " #�T�T 0 elementname elementName� 4    !�S�
�S 
prcs� o     �R�R 0 processname processName�V  � o      �Q�Q 0 appeared  � R      �P�O�N
�P .ascrerr ****      � ****�O  �N  �X  � ��� I  4 9�M��L
�M .sysodelanull��� ��� nmbr� o   4 5�K�K 	0 slice  �L  � ��� I  : ?�J��I
�J .ascrcmnt****      � ****� m   : ;�� ���  .�I  � ��H� r   @ E��� \   @ C��� o   @ A�G�G 0 timeleft timeLeft� o   A B�F�F 	0 slice  � o      �E�E 0 timeleft timeLeft�H  � G    ��� l   ��D�C� o    �B�B 0 appeared  �D  �C  � B    ��� o    �A�A 0 timeleft timeLeft� m    �@�@  � m    	���                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  � ��� I  L Q�?��>
�? .ascrcmnt****      � ****� o   L M�=�= 0 timeleft timeLeft�>  � ��<� L   R T�� o   R S�;�; 0 timeleft timeLeft�<  � ��� l     �:�9�8�:  �9  �8  � ��� l     �7�6�5�7  �6  �5  � ��� l     �4���4  � + % wait for the given element to appear   � ��� J   w a i t   f o r   t h e   g i v e n   e l e m e n t   t o   a p p e a r� ��� l     �3�2�1�3  �2  �1  � ��� i    ��� I      �0��/�0 0 waitforappear waitForAppear� ��� o      �.�. 0 element  � ��� 1      �-
�- 
time� ��,� o      �+�+ 	0 slice  �,  �/  � k     I�� ��� r     ��� 1     �*
�* 
time� o      �)�) 0 timeleft timeLeft� ��� r    ��� m    �(
�( boovfals� o      �'�' 0 appeared  � ��� W    @��� k    ;��    Q    )�& r      I   �%�$
�% .coredoexnull���     **** o    �#�# 0 element  �$   o      �"�" 0 appeared   R      �!� �
�! .ascrerr ****      � ****�   �  �&    I  * /�	�
� .sysodelanull��� ��� nmbr	 o   * +�� 	0 slice  �   

 I  0 5��
� .ascrcmnt****      � **** m   0 1 �  .�   � r   6 ; \   6 9 o   6 7�� 0 timeleft timeLeft o   7 8�� 	0 slice   o      �� 0 timeleft timeLeft�  � G     l   �� o    �� 0 appeared  �  �   B     o    �� 0 timeleft timeLeft m    ��  �  I  A F��
� .ascrcmnt****      � **** o   A B�� 0 timeleft timeLeft�   � L   G I o   G H�� 0 timeleft timeLeft�  �  l     ��
�	�  �
  �	    !  l     �"#�  "  -   # �$$  -! %&% l     �'(�  ' , &- wait for the given element to vanish   ( �)) L -   w a i t   f o r   t h e   g i v e n   e l e m e n t   t o   v a n i s h& *+* l     �,-�  ,  -   - �..  -+ /0/ i    121 I      �3�� 0 waitforvanish waitForVanish3 454 o      �� 0 element  5 676 1      �
� 
time7 8�8 o      � �  	0 slice  �  �  2 k     A99 :;: r     <=< 1     ��
�� 
time= o      ���� 0 timeleft timeLeft; >?> Q    8@A��@ V    /BCB k    *DD EFE I   ��G��
�� .sysodelanull��� ��� nmbrG o    ���� 	0 slice  ��  F HIH I   $��J��
�� .ascrcmnt****      � ****J m     KK �LL  .��  I M��M r   % *NON \   % (PQP o   % &���� 0 timeleft timeLeftQ o   & '���� 	0 slice  O o      ���� 0 timeleft timeLeft��  C F    RSR l   T����T I   ��U��
�� .coredoexnull���     ****U o    ���� 0 element  ��  ��  ��  S ?    VWV o    ���� 0 timeleft timeLeftW m    ����  A R      ������
�� .ascrerr ****      � ****��  ��  ��  ? XYX I  9 >��Z��
�� .ascrcmnt****      � ****Z o   9 :���� 0 timeleft timeLeft��  Y [��[ L   ? A\\ o   ? @���� 0 timeleft timeLeft��  0 ]^] l     ��������  ��  ��  ^ _`_ l     ��ab��  a  -   b �cc  -` ded l     ��fg��  f ! - choose a menu button item   g �hh 6 -   c h o o s e   a   m e n u   b u t t o n   i t e me iji l     ��kl��  k  -   l �mm  -j non i    pqp I      ��r���� ,0 choosemenubuttonitem chooseMenuButtonItemr sts o      ���� 0 appname appNamet uvu o      ���� 0 
buttonname 
buttonNamev w��w o      ���� 0 itemname itemName��  ��  q O     Qxyx O    Pz{z O    O|}| k    N~~ � q    �� ������ 0 win1  ��  � ��� r    ���  g    � o      ���� 0 win1  � ���� O    N��� k    M�� ��� I   (�����
�� .ascrcmnt****      � ****� b    $��� b    "��� b     ��� m    �� ���  c h o o s i n g  � o    ���� 0 itemname itemName� m     !�� ���     o f   m e n u   b u t t o n  � o   " #���� 0 
buttonname 
buttonName��  � ���� O   ) M��� k   0 L�� ��� I  0 5������
�� .prcsclicnull��� ��� uiel��  ��  � ��� I  6 ;�����
�� .sysodelanull��� ��� nmbr� m   6 7�� ?���������  � ���� O   < L��� I  C K�����
�� .prcsclicnull��� ��� uiel� 4   C G���
�� 
menI� o   E F���� 0 itemname itemName��  � 4   < @���
�� 
menE� m   > ?���� ��  � 4   ) -���
�� 
menB� o   + ,���� 0 
buttonname 
buttonName��  � 4    ���
�� 
sheE� m    ���� ��  } 4    ���
�� 
cwin� m    ���� { 4    ���
�� 
prcs� o    ���� 0 appname appNamey m     ���                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  o ��� l     ��������  ��  ��  � ��� l     ������  �   choose a popup    � ���     c h o o s e   a   p o p u p  � ��� l     ��������  ��  ��  � ��� i     #��� I      ������� 0 choosepopup choosePopUp� ��� o      ���� 0 appname appName� ��� o      ���� 0 
buttonname 
buttonName� ���� o      ���� 0 itemname itemName��  ��  � O     ���� O    ��� O    ~��� O    }��� k    |�� ��� I   $�����
�� .ascrcmnt****      � ****� b     ��� b    ��� b    ��� m    �� ���  c h o o s i n g  � o    ���� 0 itemname itemName� m    �� ���     o f   p o p   u p   m e n u  � o    ���� 0 
buttonname 
buttonName��  � ��� X   % M����� k   7 H�� ��� q   7 7�� ������ 0 pbutton1  ��  � ��� r   7 :��� o   7 8���� 0 pbutton  � o      ���� 0 pbutton1  � ���� I  ; H�����
�� .ascrcmnt****      � ****� b   ; D��� b   ; @��� n   ; >��� 1   < >��
�� 
desc� o   ; <���� 0 pbutton  � m   > ?�� ���  =� n   @ C��� 1   A C��
�� 
valL� o   @ A���� 0 pbutton  ��  ��  �� 0 pbutton  � 2  ( +��
�� 
popB� ���� O   N |��� k   ^ {�� ��� I  ^ c�����
�� .prcsclicnull��� ��� uiel�  g   ^ _��  � ��� I  d k�����
�� .sysodelanull��� ��� nmbr� m   d g�� ?�      ��  � ���� I  l {�����
�� .prcspicknull���     obj � n   l w��� 4   r w���
�� 
menI� o   u v���� 0 itemname itemName� 4   l r���
�� 
menE� m   p q���� ��  ��  � l  N [������ 6  N [� � 4 N R��
�� 
popB m   P Q����   =  S Z 1   T V��
�� 
desc o   W Y���� 0 
buttonname 
buttonName��  ��  ��  � 4    ��
�� 
sheE m    ���� � 4    ��
�� 
cwin m    ���� � 4    ��
�� 
prcs o    ���� 0 appname appName� m     �                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  � 	 l     ��������  ��  ��  	 

 l     ��������  ��  ��    l     ��   A ; https://developer.apple.com/library/archive/documentation/    � v   h t t p s : / / d e v e l o p e r . a p p l e . c o m / l i b r a r y / a r c h i v e / d o c u m e n t a t i o n /  l     �~�~   @ : LanguagesUtilities/Conceptual/MacAutomationScriptingGuide    � t   L a n g u a g e s U t i l i t i e s / C o n c e p t u a l / M a c A u t o m a t i o n S c r i p t i n g G u i d e  l     �}�}   M G AutomatetheUserInterface.html#//apple_ref/doc/uid/TP40016239-CH69-SW17    � �   A u t o m a t e t h e U s e r I n t e r f a c e . h t m l # / / a p p l e _ r e f / d o c / u i d / T P 4 0 0 1 6 2 3 9 - C H 6 9 - S W 1 7  l     �|�{�z�|  �{  �z    i   $ '  I      �y!�x�y  0 choosemenuitem chooseMenuItem! "#" o      �w�w 0 
theappname 
theAppName# $%$ o      �v�v 0 themenuname theMenuName% &�u& o      �t�t "0 themenuitemname theMenuItemName�u  �x    Q     S'()' k    I** +,+ l   �s-.�s  - ( " Bring the target app to the front   . �// D   B r i n g   t h e   t a r g e t   a p p   t o   t h e   f r o n t, 010 O    232 I  
 �r�q�p
�r .miscactvnull��� ��� null�q  �p  3 4    �o4
�o 
capp4 o    �n�n 0 
theappname 
theAppName1 565 l   �m�l�k�m  �l  �k  6 787 l   �j9:�j  9   Target the app   : �;;    T a r g e t   t h e   a p p8 <=< O    F>?> O    E@A@ k    DBB CDC l   �i�h�g�i  �h  �g  D EFE l   �fGH�f  G   Target the menu bar   H �II (   T a r g e t   t h e   m e n u   b a rF J�eJ O    DKLK k   # CMM NON l  # #�d�c�b�d  �c  �b  O PQP l  # #�aRS�a  R   Target the menu by name   S �TT 0   T a r g e t   t h e   m e n u   b y   n a m eQ U�`U O   # CVWV O   * BXYX k   1 AZZ [\[ l  1 1�_�^�]�_  �^  �]  \ ]^] l  1 1�\_`�\  _   Click the menu item   ` �aa (   C l i c k   t h e   m e n u   i t e m^ bcb I  1 8�[d�Z
�[ .ascrcmnt****      � ****d b   1 4efe m   1 2gg �hh  c l i c k i n g  f o   2 3�Y�Y "0 themenuitemname theMenuItemName�Z  c i�Xi I  9 A�Wj�V
�W .prcsclicnull��� ��� uielj 4   9 =�Uk
�U 
menIk o   ; <�T�T "0 themenuitemname theMenuItemName�V  �X  Y 4   * .�Sl
�S 
menEl o   , -�R�R 0 themenuname theMenuNameW 4   # '�Qm
�Q 
mbrim o   % &�P�P 0 themenuname theMenuName�`  L 4     �On
�O 
mbarn m    �N�N �e  A 4    �Mo
�M 
prcso o    �L�L 0 
theappname 
theAppName? m    pp�                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  = q�Kq L   G Irr m   G H�J
�J boovtrue�K  ( R      �I�H�G
�I .ascrerr ****      � ****�H  �G  ) L   Q Sss m   Q R�F
�F boovfals tut l     �E�D�C�E  �D  �C  u vwv l     �Bxy�B  x  -   y �zz  -w {|{ l     �A}~�A  } 7 1- make a new path for the given pptx or pptm file   ~ � b -   m a k e   a   n e w   p a t h   f o r   t h e   g i v e n   p p t x   o r   p p t m   f i l e| ��� l     �@���@  �  -   � ���  -� ��� i   ( +��� I      �?��>�? 0 makenewpath makeNewPath� ��=� o      �<�< 0 f  �=  �>  � k     3�� ��� r     ��� c     ��� o     �;�; 0 f  � m    �:
�: 
TEXT� o      �9�9 0 t  � ��8� Z    3���7�� G    ��� D    	��� o    �6�6 0 t  � m    �� ��� 
 . p p t x� D    ��� o    �5�5 0 t  � m    �� ��� 
 . p p t m� L    "�� b    !��� l   ��4�3� n    ��� 7   �2��
�2 
ctxt� m    �1�1 � m    �0�0��� o    �/�/ 0 t  �4  �3  � m     �� ���  p d f�7  � L   % 3�� b   % 2��� l  % 0��.�-� n   % 0��� 7  & 0�,��
�, 
ctxt� m   * ,�+�+ � m   - /�*�*��� o   % &�)�) 0 t  �.  �-  � m   0 1�� ���  p d f�8  � ��(� l     �'�&�%�'  �&  �%  �(       �$�������������$  � �#�"�!� �������
�# .aevtoappnull  �   � ****�" 0 ppt2pdf ppt2Pdf�! 0 showelement showElement�   0 showuielements showUiElements� *0 waitforappearwindow waitForAppearWindow� 0 waitforappear waitForAppear� 0 waitforvanish waitForVanish� ,0 choosemenubuttonitem chooseMenuButtonItem� 0 choosepopup choosePopUp�  0 choosemenuitem chooseMenuItem� 0 makenewpath makeNewPath� � 2�����
� .aevtoappnull  �   � ****� 0 argv  �  � �� 0 argv  �  E H�� [�
� .corecnte****       ****
� .ascrcmnt****      � ****� 0 ppt2pdf ppt2Pdf� 4�)  ��lvE�Y hO�j j O�j l 
�j Y )�k+ � � o������ 0 ppt2pdf ppt2Pdf� ��� �  �� 0 	filenames 	fileNames�  � 
�
�	���������
 0 	filenames 	fileNames�	 0 pp  � 0 isfirst  � 0 filename fileName� 0 basepath  � 0 filepath filePath� 0 pdfpath pdfPath� 0 mytitle myTitle� 0 timeleft timeLeft� 0 basepathstr basePathStr� G v�  |���������� � � ��� � ��� � ��� #��)����1p������nz}�����������������Z������������������������������!+��E��N���
�  .ascrcmnt****      � ****
�� 
capp
�� .ascrnoop****      � ****
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
bool�� 0 makenewpath makeNewPath
�� .aevtodocnull  �    alis��  0 choosemenuitem chooseMenuItem
�� 
errn����
�� 
prcs
�� 
cwin
�� 
titl�� ,0 choosemenubuttonitem chooseMenuButtonItem
�� .sysodelanull��� ��� nmbr�� �� �� *0 waitforappearwindow waitForAppearWindow
�� 
mbar
�� 
mbri
�� 
menE
�� 
menI
�� .prcsclicnull��� ��� uiel
�� 
faal
�� eMdsKcmd
�� eMdsKsft
�� .prcskprsnull���     ctxt
�� 
sheE
�� 
comB
�� 
valL
�� 
butT�� 0 waitforappear waitForAppear�� 
�� .aevtquitnull��� ��� null�C�j O�E�O*�/2*j OeE�O�[��l kh � �E�O�%j OfE�Y���
 ���&
 ���&��%�%E�O)�k+ E�O�%a %�%j O�j O)�a a m+  a j O)a a lhY hOa j Oa  *a �/a k/a ,a %E�UO)�a a m+  Okj !Oa Ha "�%j O)�a #a $a %a &+ 'E�O�j a (�%a )%j O)a a lhY	a *�%a +%j O*a a ,/ �a -j !O*a .k/a /a 0/a 1k/a 2a 3/j 4Oa %j !Oa 5a 6a 7a 8lvl 9Oa -j !O*a �/a :k/ �*a :k/ 3�a ;%E�O�*a <k/a =,FOa -j !O*a >a ?/j 4Oa -j !UO*a >a @/j 4O)*a :k/ma -m+ AE�O*a :k/ �j *a >a B/j 4Y hUOa Cj !Oa Da 6a 7kvl 9UOPUUOa Ej OPY h[OY��O*j FU� ������������ 0 showelement showElement�� ����� �  ���� 0 uielem uiElem��  � ������ 0 uielem uiElem�� 0 	classname 	className� ������,��02��
�� 
pcls
�� 
TEXT
�� 
pDSC�� 	0 value  
�� .ascrcmnt****      � ****��  ��,�&E�O��,�&�%��,%�&�%�%�%j � ��B����������  0 showuielements showUiElements�� ����� �  ������ 0 appname appName�� "0 filterclassname filterClassName��  � ������������ 0 appname appName�� "0 filterclassname filterClassName�� 0 uielem uiElem�� 0 	classname 	className�� $0 filterclasssname filterClasssname� �������������������������������z~�������
�� 
prcs
�� 
cwin�  
�� 
attr
�� 
valL
�� 
ects
�� 
list
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
pcls
�� 
TEXT
�� 
msng
�� 
bool
�� 
desc
�� .ascrcmnt****      � ****��  ��  �� �� |*�/ t*�k/�[��/�,\Ze81 ` ]*�,�&[��l kh  >��,�&E�O�� 
 �� �& $�a ,�&a %��,%�&a %�%a %j Y hW X  h[OY��UUU� ������������� *0 waitforappearwindow waitForAppearWindow�� ����� �  ���������� 0 elementname elementName�� 0 processname processName
�� 
time�� 	0 slice  ��  � �������������� 0 elementname elementName�� 0 processname processName
�� 
time�� 	0 slice  �� 0 timeleft timeLeft�� 0 appeared  � 
������������������
�� 
bool
�� 
prcs
�� 
cwin
�� .coredoexnull���     ****��  ��  
�� .sysodelanull��� ��� nmbr
�� .ascrcmnt****      � ****�� U�E�OfE�O� @ =h�
 �j�& *�/�/j E�W X  hO�j O�j 	O��E�[OY��UO�j 	O�� ������������� 0 waitforappear waitForAppear�� ����� �  �������� 0 element  
�� 
time�� 	0 slice  ��  � ������������ 0 element  
�� 
time�� 	0 slice  �� 0 timeleft timeLeft�� 0 appeared  � ������������
�� 
bool
�� .coredoexnull���     ****��  ��  
�� .sysodelanull��� ��� nmbr
�� .ascrcmnt****      � ****�� J�E�OfE�O 7h�
 �j�& �j E�W X  hO�j O�j O��E�[OY��O�j O�� ��2���������� 0 waitforvanish waitForVanish�� ����� �  �������� 0 element  
�� 
time�� 	0 slice  ��  � ����~�}�� 0 element  
� 
time�~ 	0 slice  �} 0 timeleft timeLeft� �|�{�zK�y�x�w
�| .coredoexnull���     ****
�{ 
bool
�z .sysodelanull��� ��� nmbr
�y .ascrcmnt****      � ****�x  �w  �� B�E�O - 'h�j  	 �j�&�j O�j O��E�[OY��W X  hO�j O�� �vq�u�t���s�v ,0 choosemenubuttonitem chooseMenuButtonItem�u �r��r �  �q�p�o�q 0 appname appName�p 0 
buttonname 
buttonName�o 0 itemname itemName�t  � �n�m�l�k�n 0 appname appName�m 0 
buttonname 
buttonName�l 0 itemname itemName�k 0 win1  � ��j�i�h���g�f�e��d�c�b
�j 
prcs
�i 
cwin
�h 
sheE
�g .ascrcmnt****      � ****
�f 
menB
�e .prcsclicnull��� ��� uiel
�d .sysodelanull��� ��� nmbr
�c 
menE
�b 
menI�s R� N*�/ F*�k/ >*E�O*�k/ 2�%�%�%j O*�/ *j O�j 
O*�k/ 
*�/j UUUUUU� �a��`�_���^�a 0 choosepopup choosePopUp�` �]��] �  �\�[�Z�\ 0 appname appName�[ 0 
buttonname 
buttonName�Z 0 itemname itemName�_  � �Y�X�W�V�U�Y 0 appname appName�X 0 
buttonname 
buttonName�W 0 itemname itemName�V 0 pbutton  �U 0 pbutton1  � �T�S�R���Q�P�O�N�M�L��K��J��I�H�G�F
�T 
prcs
�S 
cwin
�R 
sheE
�Q .ascrcmnt****      � ****
�P 
popB
�O 
kocl
�N 
cobj
�M .corecnte****       ****
�L 
desc
�K 
valL
�J .prcsclicnull��� ��� uiel
�I .sysodelanull��� ��� nmbr
�H 
menE
�G 
menI
�F .prcspicknull���     obj �^ �� }*�/ u*�k/ m*�k/ e�%�%�%j O '*�-[��l 
kh �E�O��,�%��,%j [OY��O*�k/�[�,\Z�81 *j Oa j O*a k/a �/j UUUUU� �E �D�C���B�E  0 choosemenuitem chooseMenuItem�D �A��A �  �@�?�>�@ 0 
theappname 
theAppName�? 0 themenuname theMenuName�> "0 themenuitemname theMenuItemName�C  � �=�<�;�= 0 
theappname 
theAppName�< 0 themenuname theMenuName�; "0 themenuitemname theMenuItemName� �:�9p�8�7�6�5g�4�3�2�1�0
�: 
capp
�9 .miscactvnull��� ��� null
�8 
prcs
�7 
mbar
�6 
mbri
�5 
menE
�4 .ascrcmnt****      � ****
�3 
menI
�2 .prcsclicnull��� ��� uiel�1  �0  �B T K*�/ *j UO� 2*�/ **�k/ "*�/ *�/ �%j O*�/j 
UUUUUOeW 	X  f� �/��.�-���,�/ 0 makenewpath makeNewPath�. �+��+ �  �*�* 0 f  �-  � �)�(�) 0 f  �( 0 t  � 	�'���&�%�$��#�
�' 
TEXT
�& 
bool
�% 
ctxt�$���#���, 4��&E�O��
 ���& �[�\[Zk\Z�2�%Y �[�\[Zk\Z�2�% ascr  ��ޭ